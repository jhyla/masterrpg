package com.ph.rpg.scene;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import com.ph.rpg.controllers.CameraController;
import com.ph.rpg.objects.MovingObject;

import java.io.IOException;
import java.util.Vector;

/**
 * Created by Hamish on 2016-05-21.
 */
public class SceneManager {

    private static final String scenesXMLPath = "scenes/scenes.xml";
    public static Vector<PHScene> scenes = new Vector();
    private static XmlReader.Element scenesXML;

    public static int currentSceneID = -1;
    public static int previousSceneID;

    static{
        try {
            XmlReader reader = new XmlReader();
            XmlReader.Element root = reader.parse(Gdx.files.internal(scenesXMLPath));

            //Animations
            scenesXML = root.getChildByName("scenes");
           /* Array<XmlReader.Element> scenesList = scenesXML.getChildrenByName("scene");
            for (XmlReader.Element child : scenesList) {
                scenes.add(new PHScene(child));
            }*/
        } catch (IOException e){}
    }

    public static PHScene getCurrentScene(){
            for(PHScene scene : scenes){
                if(scene.getId()== currentSceneID){
                    scene.pull();
                    return scene;
                }
            }
            Array<XmlReader.Element> scenesList = scenesXML.getChildrenByName("scene");
            for (XmlReader.Element child : scenesList) {
                if(Integer.parseInt(child.get("id")) == currentSceneID){
                    scenes.add(new PHScene(child));
                    return scenes.lastElement();
                }
            }

        throw new IllegalStateException();
    }

    public static void resetScenes(){
        scenes.clear();
        currentSceneID = -1;
        goToScene(1);
    }

    public static void goToScene(int i){
        previousSceneID = currentSceneID;
        currentSceneID = i;

        System.out.println("change scene from " + previousSceneID + " to " + currentSceneID);
        disposeLastScene();

        MovingObject.mainObject.setCoord(getCurrentScene().getStartPoint(previousSceneID));
        MovingObject.mainObject.setSpeed(getCurrentScene().getZoom());
        CameraController.scroll(0);
        MovingObject.mainObject.faceToward(getCurrentScene().getFacing(previousSceneID));
        MovingObject.mainObject.stop();
    }

    private static void disposeLastScene() {
        for(PHScene scene : scenes){
            if(scene.getId()== previousSceneID){
                scene.dispose();
            }
        }
    }

    public static PHGate checkGates(Color color) {
        return getCurrentScene().checkGates(color);
    }

    public static void dispose(){
        disposeLastScene();
    }
}
